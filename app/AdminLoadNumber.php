<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLoadNumber extends Model
{
    protected $fillable = ['company_id','number'];

    public function company(){
        return $this->belongsTo('\App\Company');
    }
}
