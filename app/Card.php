<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['serial_number','number','amount','company_id','img_url','date','purchased'];

    public function company(){
        return $this->belongsTo('\App\Company');
    }
}
