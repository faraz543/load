<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardTransaction extends Model
{
    protected $fillable = ['card_id','date'];
 
    public function card(){
        return $this->belongsTo('\App\Card');
    }
}
