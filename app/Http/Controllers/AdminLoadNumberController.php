<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AdminLoadNumber;
use App\Company;
use Validator;
use Session;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;


class AdminLoadNumberController extends Controller
{

    protected function LoadNumberValidator(array $data) {
        return Validator::make($data, [
                    'company_id' => 'required|integer',
                    'number' => 'required|string',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adminNumbers = AdminloadNumber::with('company')->get();
        $companies = array_column(json_decode(json_encode(Company::all()), TRUE), 'name', 'name');
        return view('adminLoadNumber.index', compact('adminNumbers', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = array_column(json_decode(json_encode(Company::all()), TRUE), 'name', 'id');
        return view('adminLoadNumber.add', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->LoadNumberValidator($request->all());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }else{
            if(AdminloadNumber::create($request->all())){
                Session::flash('Number-success','Card Addition Successfull');
                return Redirect('admin/admin-numbers');
            }else{
                Session::flash('Number-error','Card Addition Failed');
                return Redirect('admin/admin-number/add');
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adminNumber = AdminloadNumber::find($id);
        $companies = array_column(json_decode(json_encode(\App\Company::all()), TRUE), 'name', 'id');
        return view('adminLoadNumber.edit',compact('companies','adminNumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->LoadNumberValidator($request->all());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->messages());
        }else{
            if($adminNumber = AdminloadNumber::find($request->id)){
                if($adminNumber->update($request->all())){
                    Session::flash('Number-success','Number Updation Successfull');
                    return Redirect('admin/admin-numbers');
                }else{
                    Session::flash('Number-error','Number Updation Failed');
                    return Redirect('admin/admin-number/add');
                }

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adminNumber = AdminloadNumber::find($id);
        if ($adminNumber) {
            if ($adminNumber->delete()) {
                    Session::flash('Number-error', 'Deletion Successful');
                    return Redirect('admin/admin-numbers');
            } else {
                Session::flash('Number-error', 'Deletion Failed');
                return Redirect('admin/admin-numbers');
            }
        } else {
            Session::flash('Number-error', 'Deletion Failed');
            return Redirect('admin/admin-numbers');
        }
    }
}
