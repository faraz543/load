<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Card;
use app\Company;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Session;
use Validator;
use Carbon\Carbon;

class CardController extends Controller
{
    protected function ValidatedCard(array $data){
        return Validator::make($data ,[
            'number' => 'required|string',
            'serial_number' => 'required|string',
            'amount' => 'required|integer',
            'img' => 'sometimes|required|file',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = array_column(json_decode(json_encode(\App\Company::all()), TRUE), 'name', 'name');
        $cards = Card::all();
        // return $cards;
        return view('card.cardIndex',compact('companies','cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = array_column(json_decode(json_encode(\App\Company::all()), TRUE), 'name', 'id');
        return view('card.cardAdd',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $validator = $this->ValidatedCard($request->all());
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $img = $request->img;
            $img_name = $request->img->getClientOriginalName();
            $date = Carbon::now()->format('y-m-d');
            $request->merge(['date' => $date]);
            $request->merge(['img_url'=> $img_name]);
            if(Card::create($request->all())){
                $card_img_storage = Storage::disk('card')->put($img_name,File::get($img));
                Session::flash('card-success','Card Addition Successfull');
                return Redirect('admin/cards');
            }else{
                Session::flash('card-error','Card Addition Failed');
                return Redirect('admin/card/add');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = Card::find($id);
        $companies = array_column(json_decode(json_encode(\App\Company::all()), TRUE), 'name', 'id');
        return view('card.cardEdit',compact('companies','card'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->ValidatedCard($request->all());
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            if($card= Card::find($request->id)){
                $img_name = $card->img_url;
                $old_img = $img_name;
                $img = $request->img;
                if($img != null){
                    $img_name = $img->getClientOriginalName();
                    $card_img_url = $img_name;
                    $updated_img = true;
                }
                $request->merge(['img_url'=> $img_name]);
                // return $request->all();
                if($card->update($request->all())){
                    if(isset($updated_img)){
                        Storage::disk('card')->delete($old_img);
                        $card_img_storage = Storage::disk('card')->put($img_name,File::get($img));
                    }
                    Session::flash('card-success','Card Updation Successfull');
                    return Redirect('admin/cards');
                }else{
                    Session::flash('card-error','Card Updation Failed');
                }
            }else{
                Session::flash('card-error','Record Not Found');
                return Redirect('admin/card/add');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $load = Card::find($id);
        if ($load) {
            if ($load->delete()) {
                if(Storage::disk('card')->delete($load->img_url)){
                    Session::flash('card-error', 'Deletion Successful');
                    return Redirect::back();
                }else{
                    Session::flash('card-error', 'Deletion Failed');
                    return Redirect('admin/cards');
                }
            } else {
                Session::flash('card-error', 'Deletion Failed');
                return Redirect('admin/cards');
            }
        } else {
            Session::flash('card-error', 'Deletion Failed');
            return Redirect('admin/cards');
        }
    }
}
