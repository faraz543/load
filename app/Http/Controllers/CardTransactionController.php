<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

use App\CardTransaction;
use App\Card;
use App\Company;

use Carbon\Carbon;

class CardTransactionController extends Controller
{
    protected function CardTransactionValidator(array $data) {
        return Validator::make($data, [
                'amount' => 'required|integer',
                'company_id' => 'required|integer',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'here';
        $companies = array_column(json_decode(json_encode(\App\Company::all()), TRUE), 'name', 'name');
        $cardTrasactions = CardTransaction::with('card')->get();
        return view('card.cardTransactionIndex',compact('companies','cardTrasactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo 'HERE';
        $card = Card::where('amount',$request->amount)->where('company_id',$request->company_id)->where('purchased','0')->first();
        if($card){
            $validator = $this->CardTransactionValidator($request->all());
            if($validator->fails()){
                return response()->json(['errors' => $validator->messages()]);
            }else{
                $date = Carbon::now()->format('y-m-d');
                $request->merge(['card_id'=>$card->id,'date'=>$date]);
                if(CardTransaction::create($request->all())){
                    if($card->update(array('purchased' => '1'))){
                    // return response()->json(['success' => '5']);
                        $card_number = $card->number;
                        $serial_number = $card->serial_number;
                        $amount = $card->amount;
                        return response()->json(['success' => 'Your request forwarded successfully.','card_number'=>$card_number,'serial_number'=>$serial_number,'amount'=>$amount]);

                    }else{
                        return response()->json(['error' => 'These is some error with your request.']);
                    }
                }else{
                    return response()->json(['error' => 'These is some error with your request.']);
                }
            }
             return response()->json(['error'=>'CARD EXISTS']);
        }else{
            return response()->json(['error'=>'No more card available']);
        }
        return $request->amount;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return CardTransaction::with('card')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
