<?php

namespace App\Http\Controllers;
use App\Company;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Session;

class CompanyController extends Controller
{
    protected function companyValidator(array $data){
        return Validator::make($data, [
            'name'=>'required|unique:companies,name',
            // 
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        // return $companies[1]->name;
        return view('company.indexCompany',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.createCompany');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->companyValidator($request->all());
        if($validator->fails()){
            // Session::flash('validator','Fill the Form Properly');
            return Redirect::back()->withErrors($validator->messages());
        }else{
            if(Company::create($request->all())){
                Session::flash('company-success','Company Addition Successfull');
                return Redirect('admin/companies');
            }else{
                Session::flash('company-error','Company Addition Failed');
                return Redirect('admin/company/add');
            }
        }
        // print_r($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        if($company){
            return view('company.editCompany',compact('company'));
        }else{
            Session::flash('company-error','There was an error');
            return Redirect('admin/companies');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $company = Company::find($request->id);
        if($company){
            if($company->update($request->all())){
                Session::flash('company-success','Updation Successful');
                return Redirect('admin/companies');
            }else{
                Session::flash('company-error','Updation Failed');
                return Redirect('admin/companies');
            }
        }else{
            Session::flash('company-error','Company Not Found');
            return Redirect('admin/companies');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if($company){
            if($company->delete()){
                Session::flash('company-error','Deletion Successful');
                return Redirect::back();
            }else{
                Session::flash('company-error','Deletion Failed');
                return Redirect('admin/companies');
            }
        }else{
            Session::flash('company-error','Deletion Failed');
            return Redirect('admin/companies');
        }
    }
}
