<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Load;
use App\Company;
use Validator;
use Session;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Carbon\Carbon;
use App\AdminLoadNumber;
// use GuzzleHttp\Client;


class LoadController extends Controller {

    protected function loadValidator(array $data) {
        return Validator::make($data, [
                    'number' => 'required|string|min:11',
                    'company_id' => 'required|integer',
                    'amount' => 'required|integer',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $loads = Load::orderBy('id', 'dsc')->get();
        $companies = array_column(json_decode(json_encode(Company::all()), TRUE), 'name', 'name');
        return view('load.loadIndex', compact('loads', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        // $companies = array_column(json_decode(json_encode(Company::all()), TRUE), 'name', 'name');
        // return view('load.loadAdd', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        return [$request->all()];
        $validator = $this->loadValidator($request->all());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $date = Carbon::now()->format('y-m-d');
            $request->merge(['date' => $date]);

            if (Load::create($request->all())){
                if($adminNumber = AdminLoadNumber::where('company_id',$request->company_id)->first()){
                    $username = 'zargham';
                    $password = '16441';
                    $to = $adminNumber->number;
                    // return response()->json(['test' => $to]);
                    // $to = '03470564536';
                    $from = 'Huzaifa';
                    $company_name = Company::find($request->company_id)->name;
                    $message = "Mobile Number: {$request->number}
Company: {$company_name}
Amount:{$request->amount}";
                    // return response()->json(['test' => $message]);
                    $url = "http://Lifetimesms.com/plain?username=" . $username . "&password=" . $password .
                    "&to=" . $to . "&from=" . urlencode($from) . "&message=" . urlencode($message) . "";
                    $ch = curl_init();
                    $timeout = 30;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $first = explode(' :', $response)[0];
                    if($first == "OK"){
                        $msg = "Amount: {$request->amount}<br>Number: {$request->number}<br>Your request forwarded successfully.";
                        return response()->json(['success' => $msg]);
                    }else{
                        return response()->json(['error' => 'Try Again. An error occured during your request']);
                    }
                }else{
                    return response()->json(['error' => 'ADMIN NUMBER NOT FOUND']);
                }
            }
            else{
                return response()->json(['error' => 'These is some error with your request.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $load = Load::find($id);
        if ($load) {
            if ($load->delete()) {
                Session::flash('load-error', 'Deletion Successful');
                return Redirect::back();
            } else {
                Session::flash('load-error', 'Deletion Failed');
            }
        } else {
            Session::flash('load-error', 'Deletion Failed');
        }
    }

}
