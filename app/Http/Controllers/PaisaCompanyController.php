<?php

namespace App\Http\Controllers;

use Session;
use Validator;
use App\PaisaCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PaisaCompanyController extends Controller {

    protected function companyValidator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|unique:companies,name',
                        // 
        ]);
    }

    public function index() {
        $companies = PaisaCompany::orderBy('id', 'dsc')->get();
        return view('paisa-company.indexCompany', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('paisa-company.createCompany');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $this->companyValidator($request->all());
        if ($validator->fails()) {
            // Session::flash('validator','Fill the Form Properly');
            return Redirect::back()->withErrors($validator->messages());
        } else {
            if (PaisaCompany::create($request->all())) {
                Session::flash('company-success', 'Company Addition Successfull');
                return Redirect('admin/paisa-companies');
            } else {
                Session::flash('company-error', 'Company Addition Failed');
                return Redirect('admin/paisa-company/add');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = PaisaCompany::find($id);
        if($company){
            return view('paisa-company.editCompany',compact('company'));
        }else{
            Session::flash('company-error','There was an error');
            return Redirect('admin/paisa-companies');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $company = PaisaCompany::find($request->id);
        if($company){
            if($company->update($request->all())){
                Session::flash('company-success','Updation Successful');
                return Redirect('admin/paisa-companies');
            }else{
                Session::flash('company-error','Updation Failed');
                return Redirect('admin/paisa-companies');
            }
        }else{
            Session::flash('company-error','Company Not Found');
            return Redirect('admin/paisa-companies');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $company = PaisaCompany::find($id);
        if ($company) {
            if ($company->delete()) {
                Session::flash('company-error', 'Deletion Successful');
                return Redirect::back();
            } else {
                Session::flash('company-error', 'Deletion Failed');
            }
        } else {
            Session::flash('company-error', 'Deletion Failed');
        }
    }

}
