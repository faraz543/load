<?php

namespace App\Http\Controllers;

use Session;
use Validator;
use App\PaisaTransaction;
use App\PaisaCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PaisaTransactionController extends Controller {

    protected function paisaValidator(array $data,$cnic) {
        // if($cnic){
        //     return Validator::make($data, [
        //                 'paisa_company_id' => 'required|integer',
        //                 'account_number' => 'required|string',
        //                 'account_name' => 'required|string',
        //                 'cnic' => 'required|numeric',
        //                 'amount' => 'required|integer',
        //                 'date' => 'required|date',
        //     ]);
        // }else{
            return Validator::make($data, [
                        'paisa_company_id' => 'required|integer',
                        'account_number' => 'required|string',
                        'account_name' => 'required|string',
                        'amount' => 'required|integer',
                        'date' => 'required|date',
            ]);
        // }
    }

    public function index(Request $request) {
        $today = new Carbon();
        $companies = array_column(json_decode(json_encode(\App\PaisaCompany::all()), TRUE), 'name', 'name');
        $transactions = PaisaTransaction::orderBy('date', 'dsc')->get();
        return view('transaction.transactionIndex',compact('companies','transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $today = new Carbon();
        $request->merge(['date' => $today->format('Y-m-d')]);
        if( $request->cnic != 'EMPTY' ){ $cnic = true; }
        else{ $cnic = false; }
        $validator = $this->paisaValidator($request->all(),$cnic);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $new = PaisaTransaction::create($request->all());
            if ($new){
                $msg = "Amount: {$request->amount}<br>Account Number: {$request->account_number}<br>Your request forwarded successfully.";
                return response()->json(['success' => $msg]);
                }
            else{
                return response()->json(['error' => 'These is some error with your request.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
