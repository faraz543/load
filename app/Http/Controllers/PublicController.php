<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\PaisaCompany;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = array_column(json_decode(json_encode( Company::all()), TRUE), 'name', 'id');
        $paisa_companies = array_column(json_decode(json_encode( PaisaCompany::all()), TRUE), 'name', 'id');
        $easy_paisa = PaisaCompany::where('name','Easy Paisa')->first();
        $jazz_cash = PaisaCompany::where('name','Jazz Cash')->first();
        $one_load = PaisaCompany::where('name','One Load')->first();
        // return $easy_paisa->id;
        return view('public.home', compact('companies','paisa_companies','easy_paisa','jazz_cash','one_load'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function contactUs(){
        return view('public.about-us');
    }
    public function faq(){
        return view('public.faq');
    }
}
