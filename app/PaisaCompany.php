<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaisaCompany extends Model
{
    protected $fillable = ['name'];
    
}
