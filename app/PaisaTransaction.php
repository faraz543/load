<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaisaTransaction extends Model
{
    protected $fillable = ['paisa_company_id','account_number','amount','date','account_name'];
    
    public function paisaCompany(){
        return $this->belongsTo('\App\PaisaCompany');
    }
}
