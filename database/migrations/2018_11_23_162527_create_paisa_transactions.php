<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaisaTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paisa_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paisa_company_id')->unsigned();
            $table->string('account_number');
            $table->integer('amount');
            $table->string('account_name');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paisa_transactions');
    }
}
