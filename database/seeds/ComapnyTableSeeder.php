<?php

use Illuminate\Database\Seeder;

class ComapnyTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('companies')->insert(
        [['name' => 'Warid'], ['name' => 'Telenor'], ['name' => 'Jazz'], ['name' => 'Zong'], ['name' => 'Ufone']]
        );
    }

}
