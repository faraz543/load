<?php

use Illuminate\Database\Seeder;

class PaisaComapnyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paisa_companies')->insert(
        [['name' => 'Easy Paisa'], ['name' => 'Jazz Cash'],['name'=>'One Load']]
        );
    }
}
