<!-- ADMIN PANEL LOAD BLADE -->
@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
	<div class="panel panel-default">
        <div class="panel-heading">
            <h3>Edit Admin Numbers</h3>
        </div>
        <div class="panel-body">
            {{ fielderrors($errors) }}
            {{ message('Number') }}
        	{!! Form::open(['method'=>'post','action'=>'AdminLoadNumberController@update','id'=>'form-id','class'=>'form-horizontal']) !!}
            {!! Form::hidden('id',$adminNumber->id) !!}
                <div class="form-group">
                    {!! Form::label('number', 'Enter Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('number',$adminNumber->number,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('company_id', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('company_id',$companies,$adminNumber->company_id,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection