@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
@include('includes.flashmessage')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="">
            	Admin Load Number
            	<button class="btn btn-primary pull-right"><a href="{{url('/admin/admin-number/add')}}" style="color:#fff">Add Admin Number</a></button>
            </h3>
            
        </div>
        <div class="panel-body">
        	{{ fielderrors($errors) }}
            {{ message('Number') }}
        	<table class="table table-bordered text-center center" id="datatable">
		        <thead>
		            <tr>
		                <th width="20%" class="text-center">Company</th>
		                <th width="30%" class="text-center">Number</th>
		                <th width="30%" class="text-center">Actions</th>
		            </tr>
		        </thead>
		        <tbody>
					@foreach($adminNumbers as $adminNumber)
						<tr>
							<td>{{$adminNumber->company->name}}</td>
							<td>{{$adminNumber->number}}</td>
							<td><a href='{{url("/admin/admin-number/del/$adminNumber->id")}}'><i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;<a href='{{url("/admin/admin-number/edit/$adminNumber->id")}}'><i class="fa fa-pencil"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
    </div>
@endsection
@section('extra-js')
<script type="text/javascript">

</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection