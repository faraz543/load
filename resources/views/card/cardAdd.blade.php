<!-- ADMIN PANEL LOAD BLADE -->
@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
	<div class="panel panel-default">
        <div class="panel-heading">
            <h3>Add Cards In Database</h3>
        </div>
        <div class="panel-body">
            {{ fielderrors($errors) }}
            {{ message('card') }}
        	{!! Form::open(['method'=>'post','action'=>'CardController@store','id'=>'form-id','class'=>'form-horizontal','files'=>'true']) !!}
                <div class="form-group">
                    {!! Form::label('number', 'Enter Card Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('number',null,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('serial_number', 'Enter Serial Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('serial_number',null,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('amount',['null'=>'Select Amount','100'=>'100','300'=>'300','500'=>'500','1000'=>'1000'],1,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('img','Upload Image',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::file('img',['id'=>'img','class'=>'form-control','accept' => '.jpg, .png','required'=>'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('company_id', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('company_id',$companies,null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection