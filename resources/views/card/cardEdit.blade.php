<!-- ADMIN PANEL LOAD BLADE -->
@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
	<div class="panel panel-default">
        <div class="panel-heading">
            <h3>Update Cards In Database</h3>
        </div>
        <div class="panel-body">
            {{ fielderrors($errors) }}
            {{ message('card') }}
        	{!! Form::open(['method'=>'post','action'=>'CardController@update','id'=>'form-id','class'=>'form-horizontal','files'=>'true']) !!}
                <div class="form-group">
                    <label id='label-id' class='col-sm-3 control-label' >Current Card Image</label>
                    <div class="col-sm-6">
                        <div class="center col-sm-8" >
                            <img src="{{url('/'.env('CARD_IMG')).'/'.$card->img_url}}" class="card_img">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('number', 'Enter Card Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('number',$card->number,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('serial_number', 'Enter Serial Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('serial_number',$card->serial_number,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('amount',['null'=>'Select Amount','100'=>'100','300'=>'300','500'=>'500','1000'=>'1000'],"$card->amount",['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('img','Upload Image',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::file('img',['id'=>'img','class'=>'form-control','accept' => '.jpg, .png','value'=>'aa.png']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('company_id', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('company_id',$companies,$card->company_id,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::submit('Update',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
                    {!! Form::hidden('id',$card->id,['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection