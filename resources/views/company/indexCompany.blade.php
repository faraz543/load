@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3>Companies</h3>
        <a href="{{url('/admin/company/add')}}" class="btn btn-primary" style="color:#fff">Add Company</a>
    </div>
    <div class="panel-body">
        {{message('company')}}
        <table class="table table-bordered text-center center" style="width: 85%;">
            <thead>
                <tr>
                    <th width="80%" class="text-center">Category Name</th>
                    <th width="20%" class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    <td><a href='{{ url("admin/company/del/$company->id")}}'><i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;<a href='{{ url("admin/company/edit/$company->id")}}'><i class="fa fa-pencil"></i></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection