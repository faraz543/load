<!-- VALIDATOR MESSAGE FUNCTION -->
@php function fielderrors($errors){ @endphp
    @if(count($errors) > 0)
        <div class="col-md-12" style="">
            <div class='alert alert-danger alert-dismissible col-md-12' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <ul>
                    @php
                        foreach ($errors->all() as $error) {
                            echo "<li>$error</li>";
                        }
                    @endphp
                </ul>
            </div>
        </div>
    @endif
@php } @endphp

<!-- ADDITION MESSAGES -->
@php function message($var) { @endphp
    <div class="col-md-12" style="">
        @if (Illuminate\Support\Facades\Session::has("{$var}-success"))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                {{ session("{$var}-success") }}
            </div>
        @elseif(Illuminate\Support\Facades\Session::has("{$var}-error"))
                <div class='alert alert-danger alert-dismissible' role='alert'>
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    {{ session("{$var}-error") }}
                </div>
        @endif     
    </div>
@php } @endphp