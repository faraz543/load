<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Admin Panel - Load</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{url('css/custom_admin.css')}}">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        @yield('extra-css')
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            Huzaifa Mobile Shop
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('register') }}">Register</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="col-sm-12">
                <!-- SIDEBAR -->
                @if(Auth::check())
                <div class="panel panel-default col-sm-3 no-padding" id="sidebar">
                    <div class="panel-heading">Pages</div>
                    <div class="panel-body no-padding">
                        <div class="block-content">
                            <?php
                            $current = Request::path();
                            $pages = [
                                'admin' => 'Home',
                                'admin/companies' => 'Companies', 
                                'admin/loads' => 'Load', 
                                'admin/paisa-companies' => 'Paisa Companies', 
                                'admin/transactions' => 'Paisa Transactions',
                                'admin/cards'=>'Cards',
                                'admin/card/transactions'=>'Card Transactions',
                                'admin/admin-numbers'=>'Admin Load Numbers',
                            ];
                            echo "<ul class='sidebar no-padding no-margin'>";
                            ?>
                            @foreach ($pages as $href => $page) 
                            <li class = 
                                "{{ ($current == $href) ? 'active' : '' }}"
                                ><a href='/{{$href}}'>{{$page}}</a></li>
                            @endforeach
                            <?php echo "<ul/>";
                            ?>
                        </div>
                    </div>
                </div>
                @endif
                <!-- SIDEBAR(END) -->
                <div class="col-sm-9">
                    @yield('content')
                </div>
            </div>  
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        @yield('extra-js')
    </body>
</html>
