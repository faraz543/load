<!-- ADMIN PANEL LOAD BLADE -->
<!-- @extends('layouts.app')
@section('content')
@include('includes.flashmessage')
	<div class="panel panel-default">
        <div class="panel-heading">
            <h3>Add Loads</h3>
        </div>
        <div class="panel-body">
            {{ fielderrors($errors) }}
            {{ message('load') }}
        	{!! Form::open(['method'=>'post','action'=>'LoadController@store','id'=>'form-id','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('number', 'Enter Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('number',null,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('company', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::select('company',$companies,null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::number('amount',null,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection -->