@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Add Paisa Company</h3>
        </div>
        <div class="panel-body">
            {{ fielderrors($errors) }}
            {{ message('company') }}
            {!! Form::open(['method'=>'post','action'=>'PaisaCompanyController@store','id'=>'form-id','class'=>'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Company Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                    {!! Form::text('name',null,['class'=>'class form-control','id'=>'id','validated'=>'validated']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                    <div class="col-sm-6"> 
                    {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection