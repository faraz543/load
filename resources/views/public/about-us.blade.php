@extends('layouts.public')
@section('content')
    <div id="about-us" class="col-sm-12">
        <div class="col-sm-12" id="main-content">
        	<div class="col-sm-12 about-portion">
	        	<div class="col-sm-6" id="left">
	        		<h3>About Us</h3>
	        		<p>It doesn’t matter if your family lives on the other side of the world, you’re traveling or your best friend is doing an internship abroad, we make sure you can stay in touch with everybody. This is why at Recharge.com, you can refill your own credit and that of your friends and family worldwide. Currently, we’re selling credit from over 400 carriers in more than 140 countries.</p>
	        		<div class="col-sm-3 no-padding">
	        			<img src="/img/about-people.png" width="100%">
	        		</div>
	        		<div class="col-sm-9 no-padding">
	        			<ul class="people">
	        				<li>EVERY 10 SECONDS A REFILL TAKES PLACE</li>
							<li>MORE THAN 800.000 PEOPLE USE RECHARGE.COM</li>
							<li>CUSTOMERS GIVE RECHARGE.COM AN AVERAGE OF 8</li>
	        			</ul>
	        		</div>
	        		<h3>WHY CHOOSE US?</h3>
	        		<ul class="why-recharge">
	        			<li>We’re always available for your refill needs, 24/7</li>
						<li>We offer safe payment methods</li>
						<li>You can order within 1 minute</li>
						<li>The credit will be added to the phone within 1 minute</li>
						<li>Over 600 carriers in more than 140 countries</li>
	        		</ul>
	        	</div>
	        	<div class="col-sm-6" id="right">
	        		<img src="/img/about-company.png" width="100%">
	        		<h3>OUR HISTORY</h3>
	        		<p>Creative Group is the company behind Recharge.com. The company was founded around 2002 by two friends, Dirk Ueberbach and Robin Weesie. Since then, different websites were launched, including Recharge.com in 2013. Nowadays those two friends can count on a group of 30 enthusiastic colleagues. From the Netherlands we’re ready to provide smiles worldwide. </p>
	        	</div>
        	</div>
        	<div class="col-sm-12 our-partners">
        		<h3>Our Partners</h3>
        		<img src="/img/about-partners.png" width="100%">
        	</div>
    	</div>
    </div>
@endsection