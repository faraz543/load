@extends('layouts.public')
@section('content')
    <div id="faq" class="col-sm-12 faq">
        <div class="col-sm-12" id="main-content">
        	<h3 class="faq-page-title">FAQ / FREQUENTLY ASKED QUESTIONS</h3>
        	<p class="faq-pageline">Below you can find our most frequently asked questions.</p>
        	<div class="col-sm-12">
	        	<div class="col-sm-6">
	        		<h4 class="faq-heading">The First Steps at Recharge.com</h4>
	        		<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#firstStepCollapse1">
								How to check your Warid balance</a>
								</h4>
							</div>
							<div id="firstStepCollapse1" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Enter *100# followed by the send button
										Attention: it can take up to 15 minutes until Warid has processed your recharge</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#firstStepCollapse2">
								How to contact Warid</a>
								</h4>
							</div>
							<div id="firstStepCollapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Mail Warid</li>
										<li>Call 321 from your Warid number in Pakistan</li>
										<li>Call 1111 113 21 from any other phone</li>
										<li>Visit the Warid website</li>
										<li>Visit the Warid Facebook page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#firstStepCollapse3">
								Warid tariffs</a>
								</h4>
							</div>
							<div id="firstStepCollapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Visit the Warid tariffs page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#firstStepCollapse4">
								How does Recharge.com work?</a>
								</h4>
							</div>
							<div id="firstStepCollapse4" class="panel-collapse collapse">
								<div class="panel-body">
								<p>Recharge phone credit or data in three simple steps</p>
								<ol>
									<li>Select a product<br>
									Let us know which product you’re going to buy</li>

									<li>Fill in your information<br>
									Fill in your email address and/or the recipient’s phone number</li>

									<li>Pay and receive<br>
									After your payment the order will be sent immediately!</li>
								</ol>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
								<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#firstStepCollapse5">
								Make safe and secure payments </a>
								</h4>
							</div>
							<div id="firstStepCollapse5" class="panel-collapse collapse">
								<div class="panel-body">
									We make sure the information you give is secured<br>
								Privacy and safety are very important to us at Recharge.com. That’s why there’s a Privacy Policy and an explanation on how safe Recharge.com is. You can pay online without worrying!
								</div>
							</div>
						</div>
					</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<h4 class="faq-heading">Recharge</h4>
	        		<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#rechargeCollapse1">
									How to check your Warid balance</a>
								</h4>
							</div>
							<div id="rechargeCollapse1" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Enter *100# followed by the send button
										Attention: it can take up to 15 minutes until Warid has processed your recharge</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#rechargeCollapse2">
								How to contact Warid</a>
								</h4>
							</div>
							<div id="rechargeCollapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Mail Warid</li>
										<li>Call 321 from your Warid number in Pakistan</li>
										<li>Call 1111 113 21 from any other phone</li>
										<li>Visit the Warid website</li>
										<li>Visit the Warid Facebook page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#rechargeCollapse3">
								Warid tariffs</a>
								</h4>
							</div>
							<div id="rechargeCollapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Visit the Warid tariffs page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#rechargeCollapse4">
								How does Recharge.com work?</a>
								</h4>
							</div>
							<div id="rechargeCollapse4" class="panel-collapse collapse">
								<div class="panel-body">
								<p>Recharge phone credit or data in three simple steps</p>
								<ol>
									<li>Select a product<br>
									Let us know which product you’re going to buy</li>

									<li>Fill in your information<br>
									Fill in your email address and/or the recipient’s phone number</li>

									<li>Pay and receive<br>
									After your payment the order will be sent immediately!</li>
								</ol>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
								<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#rechargeCollapse5">
								Make safe and secure payments </a>
								</h4>
							</div>
							<div id="rechargeCollapse5" class="panel-collapse collapse">
								<div class="panel-body">
									We make sure the information you give is secured<br>
								Privacy and safety are very important to us at Recharge.com. That’s why there’s a Privacy Policy and an explanation on how safe Recharge.com is. You can pay online without worrying!
								</div>
							</div>
						</div>
					</div>
	        	</div>
        	</div>
        	<div class="col-sm-12">
	        	<div class="col-sm-6">
	        		<h4 class="faq-heading">Placing an Order</h4>
	        		<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#placingOrderCollapse1">
									How to check your Warid balance</a>
								</h4>
							</div>
							<div id="placingOrderCollapse1" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Enter *100# followed by the send button
										Attention: it can take up to 15 minutes until Warid has processed your recharge</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#placingOrderCollapse2">
								How to contact Warid</a>
								</h4>
							</div>
							<div id="placingOrderCollapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Mail Warid</li>
										<li>Call 321 from your Warid number in Pakistan</li>
										<li>Call 1111 113 21 from any other phone</li>
										<li>Visit the Warid website</li>
										<li>Visit the Warid Facebook page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#placingOrderCollapse3">
								Warid tariffs</a>
								</h4>
							</div>
							<div id="placingOrderCollapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Visit the Warid tariffs page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#placingOrderCollapse4">
								How does Recharge.com work?</a>
								</h4>
							</div>
							<div id="placingOrderCollapse4" class="panel-collapse collapse">
								<div class="panel-body">
								<p>Recharge phone credit or data in three simple steps</p>
								<ol>
									<li>Select a product<br>
									Let us know which product you’re going to buy</li>

									<li>Fill in your information<br>
									Fill in your email address and/or the recipient’s phone number</li>

									<li>Pay and receive<br>
									After your payment the order will be sent immediately!</li>
								</ol>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
								<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#placingOrderCollapse5">
								Make safe and secure payments </a>
								</h4>
							</div>
							<div id="placingOrderCollapse5" class="panel-collapse collapse">
								<div class="panel-body">
									We make sure the information you give is secured<br>
								Privacy and safety are very important to us at Recharge.com. That’s why there’s a Privacy Policy and an explanation on how safe Recharge.com is. You can pay online without worrying!
								</div>
							</div>
						</div>
					</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<h4 class="faq-heading">Payment</h4>
	        		<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#paymentCollapse1">
									How to check your Warid balance</a>
								</h4>
							</div>
							<div id="paymentCollapse1" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Enter *100# followed by the send button
										Attention: it can take up to 15 minutes until Warid has processed your recharge</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#paymentCollapse2">
								How to contact Warid</a>
								</h4>
							</div>
							<div id="paymentCollapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Mail Warid</li>
										<li>Call 321 from your Warid number in Pakistan</li>
										<li>Call 1111 113 21 from any other phone</li>
										<li>Visit the Warid website</li>
										<li>Visit the Warid Facebook page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#paymentCollapse3">
								Warid tariffs</a>
								</h4>
							</div>
							<div id="paymentCollapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<ul>
										<li>Visit the Warid tariffs page</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#paymentCollapse4">
								How does Recharge.com work?</a>
								</h4>
							</div>
							<div id="paymentCollapse4" class="panel-collapse collapse">
								<div class="panel-body">
								<p>Recharge phone credit or data in three simple steps</p>
								<ol>
									<li>Select a product<br>
									Let us know which product you’re going to buy</li>

									<li>Fill in your information<br>
									Fill in your email address and/or the recipient’s phone number</li>

									<li>Pay and receive<br>
									After your payment the order will be sent immediately!</li>
								</ol>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
								<a data-toggle="collapse" class="faq-title" data-parent="#accordion" href="#paymentCollapse5">
								Make safe and secure payments </a>
								</h4>
							</div>
							<div id="paymentCollapse5" class="panel-collapse collapse">
								<div class="panel-body">
									We make sure the information you give is secured<br>
								Privacy and safety are very important to us at Recharge.com. That’s why there’s a Privacy Policy and an explanation on how safe Recharge.com is. You can pay online without worrying!
								</div>
							</div>
						</div>
					</div>
	        	</div>
        	</div>
    	</div>
    </div>
@endsection
@section('extra-js')

@endsection