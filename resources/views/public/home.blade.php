@extends('layouts.public')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.css" integrity="sha256-ke5yDzwl7GsgnYgBnCDiWSNA/x/hyU89VDHl/R535dw=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" integrity="sha256-Tcd+6Q3CIltXsx0o/gYhPNbEkb3HJJpucOvQA7csVwI=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha256-ENFZrbVzylNbgnXx0n3I1g//2WeO47XxoPe0vkp3NC8=" crossorigin="anonymous" />
@endsection
@section('content')
@include('includes.flashmessage')
<div id="inner-content" class="col-sm-12 no-padding">
    {{ fielderrors($errors) }}
    {{ message('load') }}
    <div class="row col-sm-11 center all-modals">
        <div class="modal-button-div">
            <button type="button" class="col-sm-12  col-xs-12 modal-button btn" data-toggle="modal" data-target="#jazzcashModal"><h3 style="margin: 10px 0px;color: #fff;">JazzCash&nbsp;&nbsp;<i class="fa fa-money "></i></h3></button>
        </div>
        <div class="modal-button-div">
            <button type="button" class="col-sm-12  col-xs-12 modal-button btn" data-toggle="modal" data-target="#easypaisaModal" ><h3 style="margin: 10px 0px;color: #fff;">EasyPaisa&nbsp;&nbsp;<i class="fa fa-money"></i></h3></button>
        </div>
        <div class="modal-button-div">
            <button type="button" class="col-sm-12  col-xs-12 modal-button btn b" data-toggle="modal" data-target="#cardModal" ><h3 style="margin: 10px 0px;color: #fff;">Mobile Cards&nbsp;&nbsp;<i class="fa fa-credit-card-alt"></i></h3></button>
        </div>
        <div class="modal-button-div ">
            <button type="button" class="col-sm-12  col-xs-12 modal-button btn" data-toggle="modal" data-target="#loadModal"><h3 style="margin: 10px 0px;color: #fff;">EasyLoad&nbsp;&nbsp;<i class="fa fa-mobile "></i></h3></button>
        </div>
        <div class="modal-button-div">
            <button type="button" class="col-sm-12  col-xs-12 modal-button btn" data-toggle="modal" data-target="#oneloadModal" ><h3 style="margin: 10px 0px;color: #fff;">Oneload&nbsp;&nbsp;<i class="fa fa-mobile "></i></i></h3></button>
        </div>
    </div>
    <div class="top-add-div" id="">
        <img src="/img/top-add.jpg" class="top-add">
        <div class="companies-hover-buttons col-sm-12">
            <div class="col-sm-10 center">
                <div class="hover-buttons"><img src="/img/jazz.jpg" ></div>
                <div class="hover-buttons"><img src="/img/zong.jpg" ></div>
                <div class="hover-buttons"><img src="/img/telenor.jpg" ></div>
                <div class="hover-buttons"><img src="/img/ufone.jpg" ></div>
                <div class="hover-buttons"><img src="/img/warid.jpg" ></div>
            </div>
        </div>
    </div> 
    <div class="col-sm-12 " id="">
    </div>
</div>

<!-- LOAD Modal -->
<div id="loadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">Get Load Now</h3>
            </div>
            {!! Form::open(['method'=>'post','action'=>'LoadController@store','id'=>'load-form','class'=>'form-horizontal']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('number', 'Enter Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('number',null,['class'=>'class form-control','id'=>'number','maxlength'=>'11', 'size'=>'11']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('company_id', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('company_id',$companies,null,['class'=>'form-control','id'=>'company_id']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('amount',null,['class'=>'class form-control','id'=>'amount_load']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Get Load',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- LOAD MODAL(END)  -->

<!-- Card Modal -->
<div id="cardModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">Get Your Cards Now</h3>
            </div>
            {!! Form::open(['method'=>'post','action'=>'CardTransactionController@store','id'=>'card-form','class'=>'form-horizontal']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12" id="card-modal-view" >
                            <h4 id="purchase-card">Purchase Card to Get Serial Number</h4>
                            <div class="col-sm-12" id="check-img" style="display:none">
                                <div style="margin:0px auto;float:none;width: 110px">
                                    <img src="/img/check.png" align="center">
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" id="card-number">   
                                <div class="col-sm-6">
                                    <p>Card Number</p>
                                </div>
                                <div class="col-sm-6">
                                    <p id="card-number-val">XXXXXXXXXXXX</p>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding" id="card-amount">   
                                <div class="col-sm-6">
                                    <p>Amount</p>
                                </div>
                                <div class="col-sm-6">
                                    <p id="card-amount-val">XXXX</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <p id="recharge-statement" style="display:none">Copy your Card number and recharge it</p>
                            </div>
                        </div>
                        <div class="col-sm-12 no-padding" id="another-card" align-content="center">
                            <div class="col-sm-6 col-xs-8 center">
                                <p class="btn btn-primary col-sm-12" id="another-card-btn" style="display:none">
                                    Want Another Card?
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-12 no-padding" id="card-modal-field" >
                            <div class="form-group">
                                {!! Form::label('company', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('company_id',$companies,null,['class'=>'form-control','id'=>'card_company_id']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('company', 'Select Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('amount',['100'=>'100','300'=>'300','500'=>'500','1000'=>'1000'],null,['class'=>'form-control','id'=>'card_amount']) !!}
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Get Card',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- Card MODAL(END)  -->

<!-- Easy Paisa Modal -->
<div id="easypaisaModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">Easy Paisa</h3>
            </div>
            {!! Form::open(['method'=>'post','id'=>'easypaisa-form','class'=>'form-horizontal paisa-form']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        {!! Form::hidden('paisa_company_id',$easy_paisa->id,['id'=>'easypaisa_company_id']) !!}
                        <div class="form-group jazz-acc-number-form-groups">
                            {!! Form::label('name', 'Holder Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name',null,['class'=>'class form-control','id'=>'easypaisa_name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('account_number', 'Acc. Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('account_number',null,['class'=>'class form-control','id'=>'easypaisa_account_number']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('amount',null,['class'=>'class form-control','id'=>'easypaisa_amount']) !!}
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-sm-12"> 
                            
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Get Easy Paisa',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- Easy Paisa MODAL(END)  -->
<!--  jazzCashModal Modal -->
<div id="jazzcashModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">Jazz Cash</h3>
            </div>
            {!! Form::open(['method'=>'post','action'=>'PaisaCompanyController@store','id'=>'jazzcash-form','class'=>'form-horizontal paisa-form']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        {!! Form::hidden('paisa_company_id',$jazz_cash->id,['id'=>'jazzcash_company_id']) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Holder Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name',null,['class'=>'class form-control','id'=>'jazzcash_name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('account_number', 'Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('account_number',null,['class'=>'class form-control','id'=>'jazzcash_account_number']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('amount',null,['class'=>'class form-control','id'=>'jazzcash_amount']) !!}
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-sm-12"> 
                            
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Get JazzCash',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!--  jazzCashModal MODAL(END)  -->
<!--  oneload Modal -->
<div id="oneloadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">One Load</h3>
            </div>
            {!! Form::open(['method'=>'post','action'=>'PaisaCompanyController@store','id'=>'oneload-form','class'=>'form-horizontal paisa-form']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        {!! Form::hidden('paisa_company_id',$one_load->id,['id'=>'oneload_company_id']) !!}
                        <div class="form-group jazz-acc-number-form-groups">
                            {!! Form::label('name', 'Holder Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name',null,['class'=>'class form-control','id'=>'oneload_name']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('account_number', 'Acc. Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('account_number',null,['class'=>'class form-control','id'=>'oneload_account_number']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('amount', 'Enter Amount',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('amount',null,['class'=>'class form-control','id'=>'oneload_amount']) !!}
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-sm-12"> 
                            
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Get OneLoad',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!--  oneload MODAL(END)  -->
<!-- JAZZ CARD INFO Modal -->
<div id="cardInfoModal-jazz" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center;">CARD INFO</h3>
            </div>
            {!! Form::open(['method'=>'post','id'=>'cardInfoModal-form-jazz','class'=>'form-horizontal paisa-form']) !!}
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="form-group jazz-acc-number-form-groups">
                            {!! Form::label('name', 'Name On Card',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name',null,['class'=>'class form-control','id'=>'card_info_name-jazz']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('account_number', 'Card Number',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('account_number',null,['class'=>'class form-control','id'=>'card_info_number-jazz']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('exp_date', 'Expiration Date',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('exp_date',null,['class'=>'class form-control','id'=>'card_info_exp_date-jazz']) !!}
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-sm-12"> 
                            
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">{!! Form::submit('Verify',['class'=>'btn']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- ACCOUNT INFO MODAL(END)  -->
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js" integrity="sha256-kiFgik3ybDpn1VOoXqQiaSNcpp0v9HQZFIhTgw1c6i0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function () {

    // Main Page Banner Custom CSS
    $('.top-add-div').css('height', $(window).height()-220);
    $('.top-add-div').css('max-height', $(window).height()-220);
    $('.top-add').css('height', '100%');

    // Paisa Companies Ajax Call Function
    var paisaCompanyStore = function(transaction_name){
        var amount = $('#'+transaction_name+'_amount').val();
        var account_name = $('#'+transaction_name+'_name').val();
        var account_number = $('#'+transaction_name+'_account_number').val();
        var paisa_company_id = $('#'+transaction_name+'_company_id').val();
        $.ajax({
            url: '{{env("APP_URL")}}' + '/transaction',
            type: 'post',
            data: {'_token': '{{csrf_token()}}', amount: amount, account_number: account_number, paisa_company_id: paisa_company_id,account_name: account_name},
            success(data) {
                if (data.errors !== undefined) {
                    var message = '<ul>';
                    $.each(data.errors, function (index, value) {
                        $.each(value, function (messagIndex, messageValue) {
                            message += '<li>' + messageValue + '</li>';
                        });
                    });
                    message += '<ul>';
                    toastr.error(message, 'Errors');
                    $('#cardInfoModal-form')[0].reset();
                    $('#'+transaction_name+'Modal').modal('hide');
                }
                if (data.success !== undefined) {
                    toastr.success(data.success, 'Confirmed');
                    $('#'+transaction_name+'-form')[0].reset();
                    $('#'+transaction_name+'Modal').modal('hide');
                    $('#cardInfoModal-form')[0].reset();
                }
                if (data.error !== undefined) {
                    toastr.error(data.error, 'Error');
                }
            }
        });
    };

    // Card Info Model
    // var cardModalFunction = function(transaction_name){
    //     $('#cardInfoModal').modal('show');
    //     $('#cardInfoModal-form').submit(function (e,transaction_name) {
    //         e.preventDefault();
    //         if($('#card_info_name').val() == 'test' && $('#card_info_number').val() == '100'){
    //             paisaCompanyStore(transaction_name);
    //             // $('#cardInfoModal-form')[0].reset();
    //             toastr.success('success', 'Confirmed');
    //             // $('#cardInfoModal').modal('hide');
    //         }else{
    //             console.log('Enter card name = test && number = 100','Error');
    //         }
    //     });
    // };

    // Card Verifiction method
    var cardVerify = function(transaction_name){
        if($('#card_info_name').val() == 'test' && $('#card_info_number').val() == '100'){
                paisaCompanyStore(transaction_name);
                // $('#cardInfoModal-form')[0].reset();
                toastr.success('success', 'Confirmed');
                // $('#cardInfoModal').modal('hide');
            }else{
                console.log('Enter card name = test && number = 100','Error');
            }
    }

    $('#easypaisa-form').submit(function (e) {
        e.preventDefault();
        var transaction_name = 'easypaisa';
        $('#easypaisaModal').modal('hide');
        $('#cardInfoModal').modal('show');
        $('#cardInfoModal-form').submit(function (e) {
            e.preventDefault();
            if($('#card_info_name').val() == 'test' && $('#card_info_number').val() == '100'){
                paisaCompanyStore(transaction_name);
                $('#cardInfoModal-form')[0].reset();
                toastr.success('success', 'Confirmed');
                $('#cardInfoModal').modal('hide');
            }else{
                toastr.error('Enter card name = test && number = 100','Error');
            }
        });
        // cardModalFunction(transaction_name);
    });

    $('#jazzcash-form').submit(function (e) {
        e.preventDefault();
        var transaction_name = 'jazzcash';
        $('#jazzcashModal').modal('hide');
        $('#cardInfoModal-jazz').modal('show');
        $('#cardInfoModal-form-jazz').submit(function (e) {
            e.preventDefault();
            if($('#card_info_name-jazz').val() == 'test' && $('#card_info_number-jazz').val() == '100'){
                // alert('here');
                paisaCompanyStore(transaction_name);
                $('#cardInfoModal-form')[0].reset();
                toastr.success('success', 'Confirmed');
                // $('#cardInfoModal').modal('hide');
            }else{
                // alert('no');
                toastr.error('Enter card name = test && number = 100','Error');
            }
        });
        // cardModalFunction(transaction_name);
    });

    $('#oneload-form').submit(function (e) {
        e.preventDefault();
        var transaction_name = 'oneload';
        $('#oneloadModal').modal('hide');
        $('#cardInfoModal').modal('show');
        $('#cardInfoModal-form').submit(function (e) {
            e.preventDefault();
            if($('#card_info_name').val() == 'test' && $('#card_info_number').val() == '100'){
                paisaCompanyStore(transaction_name);
                // $('#cardInfoModal-form')[0].reset();
                toastr.success('success', 'Confirmed');
                // $('#cardInfoModal').modal('hide');
            }else{
                toastr.error('Enter card name = test && number = 100','Error');
            }
        });
        // cardModalFunction(transaction_name);
    });

    $('#load-form').submit(function (e) {
        e.preventDefault();
        var amount = $('#amount_load').val();
        var number = $('#number').val();
        var company_id = $('#company_id').val();
        $.ajax({
            url: '{{env("APP_URL")}}' + '/load',
            type: 'post',
            data: {'_token': '{{csrf_token()}}', amount: amount, number: number, company_id: company_id},
            success(data) {
                if (data.errors !== undefined) {
                    var message = '<ul>';
                    $.each(data.errors, function (index, value) {
                        $.each(value, function (messagIndex, messageValue) {
                            message += '<li>' + messageValue + '</li>';
                        });
                    });
                    message += '<ul>';
                    toastr.error(message, 'Errors');
                }
                if (data.success !== undefined) {
                    toastr.success(data.success, 'Confirmed');
                    $('#load-form')[0].reset();
                    $('#loadModal').modal('hide');
                }
                if (data.error !== undefined) {

                    toastr.error(data.error, 'Error');
                    // console.log(data.error);
                }
                if(data.test !== undefined){
                    console.log(data.test)
                }
            }
        });
    });
    $('#card-form').submit(function (event) {
        event.preventDefault();
        var amount = $('#card_amount').val();
        var company_id = $('#card_company_id').val();
        $.ajax({
            url: '{{env("APP_URL")}}' + '/card/transaction',
            type: 'POST',
            data: {'_token': '{{csrf_token()}}', amount: amount, company_id: company_id},
            success: function(data) {
                if (data.errors !== undefined) {
                    var message = '<ul>';
                    $.each(data.errors, function (index, value) {
                        $.each(value, function (messagIndex, messageValue) {
                            message += '<li>' + messageValue + '</li>';
                        });
                    });
                    message += '<ul>';
                    toastr.error(message, 'Errors');
                }
                if (data.success !== undefined) {
                    toastr.success(data.success, 'Confirmed');
                    $('#card-form')[0].reset();
                    // $('#cardModal').modal('hide');
                    if(data.card_number !== undefined && data.serial_number !== undefined && data.amount !== undefined){
                        $('#card-modal-field').hide();
                        $('#another-card-btn').show();
                        $('#purchase-card').html('You have Successfully ordered a card');
                        $('#card-number-val').html(data.card_number);
                        $('#card-amount-val').html('Rs. '+data.amount);
                        $('#recharge-statement').show();
                        $('#check-img').show();
                    }
                }
                if (data.error !== undefined) {
                    toastr.error(data.error, 'Error');
                }
            }
        });
    });
    $('#another-card-btn').on('click',function(event){
        $('#purchase-card').html('Purchase Card to Get Serial Number');
        $('#card-number-val').html('XXXXXXXXXXXX');
        $('#card-amount-val').html('XXXX');
        $('#card-modal-field').show();
        $('#another-card-btn').hide();
        $('#recharge-statement').hide();
        $('#check-img').hide();
    });
    $('#jazz-send-on-number').on('click',function(event){
        $('#jazzcash_cnic').val('EMPTY');
        $('#jazz-cnic-form-group').hide();
        $('.jazz-acc-number-form-groups').show();
    });
    $('#jazz-send-on-cnic').on('click',function(event){
        $('#jazzcash_cnic').val(null);
        $('.jazz-acc-number-form-groups').show();
        $('#jazz-cnic-form-group').show();
    });


});

</script>
@endsection