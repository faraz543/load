@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
@include('includes.flashmessage')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="">Paisa Transaction Record</h3>
        <!-- <button class="btn btn-primary pull-right"><a href="{{url('/admin/load-add')}}" style="color:#fff">Add Load</a></button> -->
        {!! Form::open(['method'=>'post','id'=>'form-id','class'=>'form-horizontal ']) !!}
        <div class="form-group">
            {!! Form::label('company', 'Select Company',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('sel-company',$companies,null,['class'=>'form-control sel-company','placeholder'=>'All','style'=>'width:70%;']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('from', 'Date From',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('from',null,['class'=>'form-control date_range_filter date','placeholder'=>'Select Record From','id'=>'datepicker_from','style'=>'width:70%;float:left']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('To', 'Date To',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('from',null,['class'=>'form-control date_range_filter date','placeholder'=>'Select Record To','id'=>'datepicker_to','style'=>'width:70%;float:left']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        </form>
    </div>
    <div class="panel-body">
        {{message('load')}}
        <!-- {!! Form::open(['method'=>'post','id'=>'form-id','class'=>'form-horizontal ']) !!}
        <div class="form-group">
                    <div class="col-sm-6">
                    {!! Form::text('company-field',null,['class'=>'form-control date_range_filter date','placeholder'=>'Company Name','id'=>'company-field','style'=>'width:70%;float:left']) !!}
                    </div>
            </div>
    {!! Form::close() !!} -->
        <table class="table table-bordered text-center center" id="datatable">
            <thead>
                <tr>
                    <th width="20%" class="text-center">Date</th>
                    <th width="30%" class="text-center">Account Number</th>
                    <th width="20%" class="text-center">Company</th>
                    <th width="10%" class="text-center">Amount</th>
                    <!-- <th width="10%" class="text-center">Action</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($transactions as $transaction)
                <tr>
                    <td>{{$transaction->date}}</td>
                    <td>{{$transaction->account_number}}</td>
                    <td>{{$transaction->paisaCompany->name}}</td>
                    <td>{{$transaction->amount}}</td>
                    <!-- <td><button><a href='{{ url("admin/load-del/$transaction->id")}}'>Delete</a></button></td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('extra-js')
<script type="text/javascript">

</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    // Sc
    $(document).ready(function () {
        $('.sel-company').change(function () {
            var search = $(this).val();
            $('#datatable').DataTable().search($(this).val()).draw();

        });
        // $('#company-field').keyup(function(){
        // 	// alert($(this).val());
        // 	var filteredData = $('#datatable').DataTable().column(3).data().filter( function ( value,index ) {
        // 	        return value == $('#company-field').val() ? true : false;
        // 	    } );
        // });


        /* Custom filtering function which will search data in column four between two values */






        $(function () {
            var oTable = $('#datatable').DataTable({
                "oLanguage": {
                    "sSearch": "Filter Data"
                },
                "iDisplayLength": -1,
                "sPaginationType": "full_numbers",
            });

            $("#datepicker_from").datepicker({
                showOn: "button",
                buttonText: '<i class="fa fa-calendar fa-2x"></i>',
                dateFormat: 'yy-mm-dd',
                buttonImageOnly: false,
                "onSelect": function (date) {
                    minDateFilter = new Date(date).getTime();
                    oTable.draw();
                }
            }).keyup(function () {
                minDateFilter = new Date(this.value).getTime();
                oTable.draw();
            });
            $("#datepicker_to").datepicker({
                showOn: "button",
                dateFormat: 'yy-mm-dd',
                buttonText: '<i class="fa fa-calendar fa-2x"></i>',
                buttonImageOnly: false,
                "onSelect": function (date) {
                    maxDateFilter = new Date(date).getTime();
                    oTable.draw();
                }
            }).keyup(function () {
                maxDateFilter = new Date(this.value).getTime();
                oTable.draw();
            });
        });
        // Date range filter
        minDateFilter = "";
        maxDateFilter = "";

        $.fn.dataTableExt.afnFiltering.push(
                function (oSettings, aData, iDataIndex) {
                    if (typeof aData._date == 'undefined') {
                        aData._date = new Date(aData[0]).getTime();
                    }

                    if (minDateFilter && !isNaN(minDateFilter)) {
                        if (aData._date < minDateFilter) {
                            return false;
                        }
                    }

                    if (maxDateFilter && !isNaN(maxDateFilter)) {
                        if (aData._date > maxDateFilter) {
                            return false;
                        }
                    }

                    return true;
                });
    });
</script>
@endsection