<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
// PUBLIC SITE ROUTES
Route::get('/', 'PublicController@index');
Route::get('/home', 'PublicController@index');
Route::get('/about-us', 'PublicController@contactUs');
Route::get('/faq', 'PublicController@faq');


  // Load and Transaction Store Functions
  Route::post('/transaction', 'PaisaTransactionController@store');
  Route::post('/load', 'LoadController@store');
  Route::post('/card/transaction', 'CardTransactionController@store');
  
  // TESTING OF CARD DETAILS
  // Route::get('/card/transaction/{id}', 'CardTransactionController@show');

Auth::routes();

// ADMIN ROUTES
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::get('/', 'SiteController@index');
    
    // COMPANY ROUTES
    Route::get('/companies', 'CompanyController@index');
    Route::get('/company/add', 'CompanyController@create');
    Route::post('/company', 'CompanyController@store');
    Route::get('/company/edit/{id}', 'CompanyController@edit');
    Route::post('/company/edit', 'CompanyController@update');
    Route::get('/company/del/{id}', 'CompanyController@destroy');

    // PAISA COMPANY ROUTES
    Route::get('/paisa-companies', 'PaisaCompanyController@index');
    Route::get('/paisa-company/add', 'PaisaCompanyController@create');
    Route::post('/paisa-company', 'PaisaCompanyController@store');
    Route::get('/paisa-company/edit/{id}', 'PaisaCompanyController@edit');
    Route::post('/paisa-company/edit', 'PaisaCompanyController@update');
    Route::get('/paisa-company/del/{id}', 'PaisaCompanyController@destroy');

    // LOAD ROUTES
    Route::get('/loads', 'LoadController@index');

    // PAISA ROUTES
    Route::get('/transactions', 'PaisaTransactionController@index');

    // CARD ROUTES
    Route::get('/cards', 'CardController@index');
    Route::get('/card/add', 'CardController@create');
    Route::post('/card', 'CardController@store');
    Route::get('/card/edit/{id}', 'CardController@edit');
    Route::post('/card/edit', 'CardController@update');
    Route::get('/card/del/{id}', 'CardController@destroy');
    Route::get('/card/transactions', 'CardTransactionController@index');

    // ADMIN LOAD NUMBER
    Route::get('/admin-numbers', 'AdminLoadNumberController@index');
    Route::get('/admin-number/add', 'AdminLoadNumberController@create');
    Route::post('/admin-number', 'AdminLoadNumberController@store');
    Route::get('/admin-number/edit/{id}', 'AdminLoadNumberController@edit');
    Route::post('/admin-number/edit/', 'AdminLoadNumberController@update');
    Route::get('/admin-number/del/{id}', 'AdminLoadNumberController@destroy');


});


